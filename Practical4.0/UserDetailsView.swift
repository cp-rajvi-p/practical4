//
//  UserDetailsView.swift
//  NewPractical3.0
//
//  Created by Rajvi on 10/08/21.
//

import UIKit

class UserDetailsView: UIViewController {
    @IBOutlet var nameOfEmail: UILabel!
    @IBOutlet var followingCount: UILabel!
    @IBOutlet var nameOfUser: UILabel!
    @IBOutlet var imageDetails: UIImageView!
   
    @IBOutlet var uiFollowers: UIView!
    @IBOutlet var followersCount: UILabel!
    var array: [User] = [User]()
    var users : User? = nil
    var selectedAuteur: User!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameOfUser.text = selectedAuteur.name
        uiFollowers.layer.cornerRadius = 10;
        let urlString = selectedAuteur.avatarUrl
        let url = URL(string: urlString)
        let data = try? Data(contentsOf: url!)
        if let imageData = data {
            let image = UIImage(data: imageData)
            self.imageDetails.image = image
        }
        else {
            self.imageDetails.image = UIImage(named: "circle")
        }
        parseJson() { data in
            self.users = data
            print("users== \(String(describing: data.followers))")
            print("users== \(String(describing: data.following))")

            DispatchQueue.main.async {
                if let following = data.following {
                    self.followingCount.text = String(following)
                }
                if let followers = data.followers {
                    self.followersCount.text = String(followers)
                }
              if let email = data.email {                    self.nameOfEmail.text = email
             }

            }

       }
    }

    func parseJson(completion: @escaping (_ userdetails : User) -> ()) {
        let request = URLRequest(url: URL(string: selectedAuteur.url)!)
        print("request = \(request)")
        let _: Void = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data1 = data else { return }
            print(data1)
            do {
                let decoder = JSONDecoder()
                let userDetailsData = try decoder.decode(User.self, from: data1)
                print("userDetailsData == \(userDetailsData)")
                completion(userDetailsData)
            } catch let err {
                print("Err", err)
            }
        }.resume()
    }

}
