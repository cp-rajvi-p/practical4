//
//  Person.swift
//  Practical4.0
//
//  Created by Rajvi on 21/10/21.
//

import Foundation

struct Person: Identifiable {
    var id = UUID()
    
    var profileImage: Data
}
