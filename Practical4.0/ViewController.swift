//
//  ViewController.swift
//  CollectionViewBooking
//
//  Created by Bogdan on 15/7/20.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var _selectedCells = [IndexPath]()
    var db = DBManager()
    @IBOutlet var deletebtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    private lazy var numberOfElementsInRow = 4
    enum TableSection: Int {
        case userList
        case loader
    }
    
    private var numbers = [Int]()
    private let pageLimit = 10
    private var currentLastId: Int? = nil
    private var currentLastId2: Int? = nil
    var sectionss = [User]()
    var emps = Array<User>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    var data : [[User]] = [[User]]()
    var emps2 = Array<User>(){
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    var empsnew : [[User]] = [[User]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emps = db.read()
        emps2 = db.read()
        configureCollectionView()
        
        //        emps = db.read()
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let collection = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
        collectionView.dragInteractionEnabled = true
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
    }
    
    @IBAction func saveBtn(_ sender: UIButton) {
        
    }
    
    private func fetchJSONData(completed: ((Bool) -> Void)? = nil) {
        GithubAPIJSON.shared.getUsers(perPage: pageLimit, sinceId: currentLastId) { [weak self] result in
            switch result {
            case .success(let users):
                self?.emps.append(contentsOf: users)
                self?.currentLastId = users.last?.id
                completed?(true)
            case .failure(let error):
                print(error.localizedDescription)
                completed?(false)
            }
        }
    }
    
    private func fetchJSONData2(completed: ((Bool) -> Void)? = nil) {
        GithubAPIJSON.shared.getUsers(perPage: pageLimit, sinceId: currentLastId2) { [weak self] result in
            switch result {
            case .success(let users2):
                self?.empsnew[(self?.collectionView.tag)!].append(contentsOf: users2)
                self?.currentLastId2 = users2.last?.id
                completed?(true)
            case .failure(let error):
                print(error.localizedDescription)
                completed?(false)
            }
        }
    }
    private func showAlert(title: String? = nil, message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "close", style: .default, handler: nil)
        alert.addAction(close)
        present(alert, animated: true)
    }
    
    @IBAction func deletebtn(_ sender: UIBarButtonItem) {
        var removeItems = [IndexPath]()
        //                for section in 0..<emps.count {
        //                    for item in  0..<emps[section].count {
        if let _selectedCells = collectionView.indexPathsForSelectedItems {
            let items = _selectedCells.map { $0.item }
            //                emps = db.read()
            emps2 = db.read()
            for item in items {
                emps.remove(at: item - 1 )
                collectionView.deleteItems(at: [IndexPath(row: 1, section: 0)])
            }
            
            collectionView.reloadData()
            //                        }
            //                    }
        }
        removeItems.forEach { data[$0.section].remove(at: $0.item) }
        collectionView.deleteItems(at: removeItems)
        
    }
    
    func configureCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = createCompositionalLayout()
        collectionView.allowsMultipleSelection = true
        collectionView.register(FirstSection.nib, forCellWithReuseIdentifier: FirstSection.id)
        collectionView.register(SecondSection.nib, forCellWithReuseIdentifier: SecondSection.id)
        collectionView.register(SectionHeader.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,withReuseIdentifier: SectionHeader.id)
    }
    
    func createCompositionalLayout() -> UICollectionViewLayout {
        return UICollectionViewCompositionalLayout { (sectionNumber, env) -> NSCollectionLayoutSection? in
            switch sectionNumber {
            case 0:
                return self.createCollectionFirstSection()
            case 1:
                return self.createCollectionSecondSection()
            default:
                return self.createCollectionFirstSection()
            }
        }
    }
    
    func createCollectionFirstSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.31),heightDimension: .absolute(100))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .init(top: 0, leading: 0, bottom: 15, trailing: 15)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),heightDimension: .estimated(500))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets.leading = 15
        section.boundarySupplementaryItems = [
            NSCollectionLayoutBoundarySupplementaryItem(layoutSize: .init(widthDimension:.fractionalWidth(1),heightDimension:.estimated(44)), elementKind:UICollectionView.elementKindSectionHeader,alignment:.topLeading)
        ]
        return section
    }
    
    func createCollectionSecondSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.31),heightDimension: .absolute(100))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .init(top: 0, leading: 0, bottom: 15, trailing: 15)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),heightDimension: .estimated(500))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets.leading = 15
        section.boundarySupplementaryItems = [
            NSCollectionLayoutBoundarySupplementaryItem(layoutSize: .init(widthDimension:.fractionalWidth(1),heightDimension:.estimated(44)), elementKind:UICollectionView.elementKindSectionHeader,alignment:.topLeading)
        ]
        return section
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt    indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FirstSection.id, for: indexPath) as! FirstSection
        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: SecondSection.id, for: indexPath) as! SecondSection
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.firstSectionImages.startAnimating()
                fetchJSONData { [weak self] success in
                    if !success {
                    }
                }
            }
        }
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell2.SecondSectionImages.startAnimating()
                fetchJSONData2 { [weak self] success in
                    if !success {
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return  emps.count + 1
        case 1:
            return emps2.count + 1
            
        default: return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FirstSection.id, for: indexPath) as! FirstSection
            
            if indexPath.row == 0 {
                cell.firstSectionImages.image = UIImage(systemName: "plus")
            }
            else {
                //                let item = emps[indexPath.row - 1]
                let item = emps[indexPath.row - 1 ]
                db.insert(id: item.id, avatarUrl: item.avatarUrl)
                let urlString = item.avatarUrl
                let url = URL(string: urlString)
                let data = try? Data(contentsOf: url!)
                if let imageData = data {
                    let image2 = UIImage(data: imageData)
                    cell.firstSectionImages.image = image2
                }
            }
            return cell
            
        case 1:
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: SecondSection.id, for: indexPath) as! SecondSection
            
            if indexPath.row == 0 {
                cell3.SecondSectionImages.image = UIImage(systemName: "plus")
            }
            else {
                let item = emps2[indexPath.row-1]
                let urlString = item.avatarUrl
                db.insert(id: item.id, avatarUrl: item.avatarUrl)
                let url = URL(string: urlString)
                let data = try? Data(contentsOf: url!)
                if let imageData = data {
                    let image = UIImage(data: imageData)
                    cell3.SecondSectionImages.image = image
                    
                }
            }
            return cell3
        default: return UICollectionViewCell()
        }
    }
    
    private func hideBottomLoader() {
        DispatchQueue.main.async {
            let lastListIndexPath = IndexPath(row: self.emps.count - 1, section: TableSection.userList.rawValue)
            self.collectionView.scrollToItem(at: lastListIndexPath, at: .bottom, animated: true)
        }
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 20) / 3 //some width
        let height = width * 1.5 //ratio
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionHeader.id, for: indexPath) as! SectionHeader
            switch indexPath.section {
            case 0:
                header.configure(title: "Section 1")
                header.backgroundColor = UIColor.lightGray
                header.titleTxt?.textColor  = UIColor.white
                header.titleTxt?.textAlignment = .right
                header.layer.cornerRadius = 20.00
                
            case 1:
                header.configure(title: "Section 2")
                header.backgroundColor = UIColor.lightGray
                header.titleTxt?.textColor  = UIColor.white
                header.titleTxt?.textAlignment = .right
                header.layer.cornerRadius = 20.00
            default: break
            }
            return header
            
        default: return UICollectionReusableView()
        }
    }
}

extension ViewController : UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = self.emps[indexPath.item - 1]
        let urlString = item.avatarUrl
        print("urlstring == \(urlString)")
        let itemprovider = NSItemProvider(object: item.avatarUrl as NSString)
        let dragItem = UIDragItem(itemProvider: itemprovider)
        print("dragItem --- \(dragItem)")
        dragItem.localObject = item
        return [dragItem]
    }
}

extension ViewController : UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move , intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation: .forbidden)
    }
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        var destinationIndexpath : IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexpath = indexPath
        }
        else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexpath = IndexPath(item: row - 1, section: 0)
        }
        if coordinator.proposal.operation == .move {
            print("enter ")
            self.reorderItems(coordinator: coordinator, destinationIndexpath: destinationIndexpath, collectionView: collectionView)
        }
    }
    
    fileprivate func reorderItems(coordinator : UICollectionViewDropCoordinator , destinationIndexpath : IndexPath, collectionView : UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            collectionView.performBatchUpdates ({
                print("item = \(item)")
                // self.emps.remove(at: sourceIndexPath.item)
                print("destination path\(destinationIndexpath.item)")
                //  self.users.insert(item.dragItem.localObject as! String, at: destinationIndexpath.item)
                print("source index = \(sourceIndexPath.item + 1 )")
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexpath])
            },completion: nil)
            coordinator.drop(item.dragItem, toItemAt: destinationIndexpath)
        }
    }
}

